cern:
  oidc:
    enabled: true
    clientID: <path:kv/data/kubernetes/services/kops-registry/next/cern/oidc#clientID>
    clientSecret: <path:kv/data/kubernetes/services/kops-registry/next/cern/oidc#clientSecret>
    endpoint: https://auth.cern.ch/auth/realms/cern
  protected:
    enabled: true
    repos:
      - acc
      - acc-gpn
      - docker.io
      - gcr.io
      - ghcr.io
      - gitlab-registry.cern.ch
      - jeedy-sso
      - k8s.gcr.io
      - mcr.microsoft.com
      - nvcr.io
      - quay.io
      - registry.k8s.io
  tls:
    acme:
      server: https://acme-v02.api.letsencrypt.org/directory
harbor:
  core:
    image:
      repository: registry.cern.ch/kubernetes/harbor-core
      tag: v2.12.1
    resources:
      requests:
        memory: 500Mi
        cpu: 1000m
      limits:
        memory: 500Mi
        cpu: 1000m
    replicas: 3
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - podAffinityTerm:
              labelSelector:
                matchLabels:
                  app.kubernetes.io/component: core
                  app.kubernetes.io/name: harbor
              topologyKey: kubernetes.io/hostname
            weight: 1
    topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: topology.kubernetes.io/zone
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app.kubernetes.io/component: core
            app.kubernetes.io/name: harbor
    extraEnvVars:
    - name: BEEGO_MAX_MEMORY_BYTES
      value: "137438953472"
    - name: BEEGO_MAX_UPLOAD_SIZE_BYTES
      value: "137438953472"
    # Secret is used when core server communicates with other components.
    # If a secret key is not specified, Helm will generate one. Alternatively set existingSecret to use an existing secret
    secret: "<path:kv/data/kubernetes/services/kops-registry/next/harbor/core#secret>"
    # If not specifying a preexisting secret, a secret can be created from tokenKey and tokenCert and used instead.
    tokenCert: "<path:kv/data/kubernetes/services/kops-registry/next/harbor/core#tokenCert>"
    tokenKey: "<path:kv/data/kubernetes/services/kops-registry/next/harbor/core#tokenKey>"
    # The XSRF key. Will be generated automatically if it isn't specified
    xsrfKey: "<path:kv/data/kubernetes/services/kops-registry/next/harbor/core#xsrfKey>"
  database:
    external:
      coreDatabase: "registry_next"
      host: dbod-harbordb-next.cern.ch
      password: <path:kv/data/kubernetes/services/kops-registry/next/harbor/database#password>
      port: "6610"
      sslmode: require
      username: admin
    maxIdleConns: 5
    maxOpenConns: 20
    type: external
  redis:
    type: external
    external:
      addr: "kops-registry-next-redis-node-0.kops-registry-next-redis-headless:26379,kops-registry-next-redis-node-1.kops-registry-next-redis-headless:26379,kops-registry-next-redis-node-2.kops-registry-next-redis-headless:26379"
      sentinelMasterSet: "harbor"
  exporter:
    replicas: 1
    image:
      repository: registry.cern.ch/kubernetes/harbor-exporter
      tag: v2.12.1
    resources:
      requests:
        memory: 128Mi
        cpu: 200m
      limits:
        memory: 128Mi
        cpu: 200m
  expose:
    ingress:
      annotations:
        cert-manager.io/issuer: letsencrypt
        ingress.kubernetes.io/proxy-body-size: "0"
        ingress.kubernetes.io/ssl-redirect: "true"
        nginx.ingress.kubernetes.io/proxy-body-size: "0"
        nginx.ingress.kubernetes.io/proxy-request-buffering: "off"
        nginx.ingress.kubernetes.io/ssl-redirect: "true"
      hosts:
        core: registry-next.cern.ch
        notary: notary-next.cern.ch
    tls:
      certSource: secret
      enabled: true
      secret:
        secretName: harbor-next-harbor-ingress
    type: ingress
  externalURL: https://registry-next.cern.ch
  harborAdminPassword: <path:kv/data/kubernetes/services/kops-registry/next/harbor#harborAdminPassword>
  # job service dashboard shows the status of the job service queue, the job service pools, and the job service workers
  jobservice:
    image:
      repository: registry.cern.ch/kubernetes/harbor-jobservice
      tag: v2.12.1
    resources:
      requests:
        memory: 2Gi
        cpu: 1000m
      limits:
        memory: 2Gi
        cpu: 1000m
    #extraEnvVars:
    #  - name: REGISTRY_HTTP_CLIENT_TIMEOUT
    #    value: "480"
    replicas: 3
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - podAffinityTerm:
              labelSelector:
                matchLabels:
                  app.kubernetes.io/component: jobservice
                  app.kubernetes.io/name: harbor
              topologyKey: kubernetes.io/hostname
            weight: 1
    topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: topology.kubernetes.io/zone
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app.kubernetes.io/component: jobservice
            app.kubernetes.io/name: harbor
    secret: "<path:kv/data/kubernetes/services/kops-registry/next/harbor/jobservice#secret>"
  logLevel: debug
  metrics:
    enabled: true
  # The persistence is enabled by default and a default StorageClass
  # is needed in the k8s cluster to provision volumes dynamically.
  nginx:
    image:
      repository: registry.cern.ch/kubernetes/nginx-photon
      tag: v2.12.1
    replicas: 1
  notary:
    server:
      image:
        tag: v2.12.1
    signer:
      image:
        tag: v2.12.1
  persistence:
    imageChartStorage:
      s3:
        accesskey: <path:kv/data/kubernetes/services/kops-registry/next/harbor/persistence/s3-kubernetes-developers-mdc#accesskey>
        bucket: registry-next-bucket
        secretkey: <path:kv/data/kubernetes/services/kops-registry/next/harbor/persistence/s3-kubernetes-developers-mdc#secretkey>
      type: s3
    persistentVolumeClaim:
      jobservice:
        jobLog:
          existingClaim: ""
          storageClass: manila-geneva-cephfs-testing-delete
        scanDataExports:
          existingClaim: ""
          storageClass: manila-geneva-cephfs-testing-delete
      redis:
        existingClaim: ""
        storageClass: manila-geneva-cephfs-testing-delete
      trivy:
        existingClaim: ""
        storageClass: manila-geneva-cephfs-testing-delete
  portal:
    image:
      repository: registry.cern.ch/kubernetes/harbor-portal
      tag: v2.12.1
    replicas: 3
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - podAffinityTerm:
              labelSelector:
                matchLabels:
                  app.kubernetes.io/component: portal
                  app.kubernetes.io/name: harbor
              topologyKey: kubernetes.io/hostname
            weight: 1
    topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: topology.kubernetes.io/zone
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app.kubernetes.io/component: portal
            app.kubernetes.io/name: harbor
    resources:
      requests:
        memory: 128Mi
        cpu: 500m
      limits:
        memory: 128Mi
        cpu: 500m
  registry:
    registry:
      image:
        repository: registry.cern.ch/kubernetes/registry-photon
        tag: v2.12.1
    controller:
      image:
        repository: registry.cern.ch/kubernetes/harbor-registryctl
        tag: v2.12.1
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - podAffinityTerm:
              labelSelector:
                matchLabels:
                  app.kubernetes.io/component: registry
                  app.kubernetes.io/name: harbor
              topologyKey: kubernetes.io/hostname
            weight: 1
    topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: topology.kubernetes.io/zone
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app.kubernetes.io/component: registry
            app.kubernetes.io/name: harbor
    credentials:
      htpasswdString: <path:kv/data/kubernetes/services/kops-registry/next/harbor/registry#htpasswd>
      password: <path:kv/data/kubernetes/services/kops-registry/next/harbor/registry#password>
      username: admin
    replicas: 3
    resources:
      requests:
        memory: 1Gi
        cpu: 2000m
      limits:
        memory: 1Gi
        cpu: 2000m
    # Secret is used to secure the upload state from client
    # and registry storage backend.
    # If a secret key is not specified, Helm will generate one.
    secret: "<path:kv/data/kubernetes/services/kops-registry/next/harbor/registry#secret>"
  secretKey: <path:kv/data/kubernetes/services/kops-registry/next/harbor#secretKey>
  trace:
    enabled: false
  trivy:
    replicas: 1
    image:
      repository: registry.cern.ch/kubernetes/trivy-adapter-photon
      tag: v2.12.1
    affinity:
      podAntiAffinity:
        preferredDuringSchedulingIgnoredDuringExecution:
          - podAffinityTerm:
              labelSelector:
                matchLabels:
                  app.kubernetes.io/component: trivy
                  app.kubernetes.io/name: harbor
              topologyKey: kubernetes.io/hostname
            weight: 1
    topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: topology.kubernetes.io/zone
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app.kubernetes.io/component: trivy
            app.kubernetes.io/name: harbor
    resources:
      requests:
        memory: 512Mi
        cpu: 1000m
      limits:
        memory: 512Gi
        cpu: 1000m
gcSchedule: '{"parameters":{},"schedule":{"cron":"0 0 8 * * *","type":"Custom"}}'
systemSettings: '{"robot_name_prefix":"robot-","session_timeout":240}'

redis:
  enabled: true
  global:
    security:
      allowInsecureImages: true # upstream images are replicated into `registry.cern.ch`.
  image:
    registry: registry.cern.ch
    repository: kubernetes/redis
    tag: 7.4.2-debian-12-r4
  auth:
    enabled: false
    sentinel: false
  networkPolicy:
    enabled: true
    allowExternal: false
    allowExternalEgress: false
    extraIngress:
      - ports:
        - port: 6379
          from:
            - podSelector:
              - matchLabels:
                  app.kubernetes.io/component: core
                  app.kubernetes.io/name: harbor
        - port: 26379
          from:
            - podSelector:
              - matchLabels:
                  app.kubernetes.io/component: core
                  app.kubernetes.io/name: harbor
    metrics:
      allowExternal: true
  persistence:
    enabled: false
  master:
    persistence:
      enabled: false
  replica:
    replicaCount: 3
    persistence:
      enabled: false
    topologySpreadConstraints:
      - maxSkew: 1
        topologyKey: topology.kubernetes.io/zone
        whenUnsatisfiable: DoNotSchedule
        labelSelector:
          matchLabels:
            app.kubernetes.io/component: node
            app.kubernetes.io/name: redis
  sentinel:
    enabled: true
    image:
      registry: registry.cern.ch
      repository: kubernetes/redis-sentinel
      tag: 7.4.2-debian-12-r4
    masterSet: harbor
    persistence:
      enabled: false
    resources:
      requests:
        cpu: 500m
        memory: 1Gi
        ephemeral-storage: 1Gi
      limits:
        cpu: 500m
        memory: 1Gi
        ephemeral-storage: 1Gi
  metrics:
    enabled: true
    image:
      registry: registry.cern.ch
      repository: kubernetes/redis-exporter
      tag: 1.67.0-debian-12-r9
    serviceMonitor:
      enabled: true
      additionalLabels:
        release: cern-magnum
      additionalEndpoints: # scrape redis sentinel metrics & distinguish from plain redis.
        - interval: "30s"
          path: "/scrape"
          port: "http-metrics"
          params:
            target: ["localhost:26379"]
          metricRelabelings:
            - targetLabel: "app"
              replacement: "sentinel"
