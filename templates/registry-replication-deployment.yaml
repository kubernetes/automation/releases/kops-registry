{{- if .Values.cern.replicationrules }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: registry-replication-rules
  namespace: {{ .Release.Namespace }}
  labels:
    app: registry-replication
  annotations:
    checksum/config: {{ .Files.Get "data/registry-replication-rules.yaml" | sha256sum }}
data:
  registry-replication-rules.yaml: |
    {{- .Files.Get "data/registry-replication-rules.yaml" | nindent 4 }}

---
apiVersion: v1
kind: Secret
metadata:
  name: registry-replication-script
  namespace: {{ .Release.Namespace }}
  labels:
    app: registry-replication
type: Opaque
stringData:
  apply-registry-replication-rules.sh: |
    RULES="/tmp/data/registry-replication-rules.yaml"

    while : ; do
      echo ""
      echo "Get all registry replication rules"
      REPLICATION_RULES=[]
      PAGE=1
      while : ; do
        # Get page # of all registry replications
        APPEND=$(curl "{{ .Values.harbor.externalURL }}/api/v2.0/replication/policies?page=${PAGE}&page_size=25" -H 'accept: application/json' -H 'authorization: Basic {{ printf "admin:%s" .Values.harbor.harborAdminPassword | b64enc }}' --compressed --insecure)
        # Exit if no more replications
        if [ $(echo $APPEND | jq length) -eq 0 ]; then
          break
        fi
        REPLICATION_RULES=$(jq --argjson arr1 "$REPLICATION_RULES" --argjson arr2 "$APPEND" -n '$arr1 + $arr2 | group_by(.id) | map(.[-1])')
        ((PAGE+=1))
      done

      echo ""
      echo 'Get all registries (push/pull)'
      REGISTRIES=[]
      PAGE=1
      while : ; do
        # Get page # of all registry replications
        APPEND=$(curl "{{ .Values.harbor.externalURL }}/api/v2.0/registries?page=${PAGE}&page_size=25" -H 'accept: application/json' -H 'authorization: Basic {{ printf "admin:%s" .Values.harbor.harborAdminPassword | b64enc }}' --compressed --insecure)
        # Exit if no more replications
        if [ $(echo $APPEND | jq length) -eq 0 ]; then
          break
        fi
        REGISTRIES=$(jq --argjson arr1 "$REGISTRIES" --argjson arr2 "$APPEND" -n '$arr1 + $arr2 | group_by(.id) | map(.[-1])')
        ((PAGE+=1))
      done

      echo ""
      echo "Apply all defined replication rules"
      for replication_rule in $(cat $RULES | yq eval -o=json | jq -c '.replications[] | @base64' - ); do
        SRC_REPOSITORY="null"
        DEST_REPOSITORY="null"
        REPLICATION_NAME=$(echo $replication_rule | jq -r '@base64d ' | jq '.name')
        echo "Applying replication rule $REPLICATION_NAME ... "
        # Get registry replication rule ID if it exists
        REPLICATION_ID=$(echo $REPLICATION_RULES | jq ".[] | select(.name == ${REPLICATION_NAME}) | .id")
        # Prepeare data to send
        # Get src/dest registry definitions from name
        SRC_REPOSITORY=$(echo ${replication_rule} | jq -r '@base64d ' | jq '.src_registry.name')
        if [ "${SRC_REPOSITORY}" != "null" ]; then
          SRC_REPOSITORY=$(echo $REGISTRIES | jq " .[] | select(.name | contains($SRC_REPOSITORY))")
        fi
        DEST_REPOSITORY=$(echo ${replication_rule} | jq -r '@base64d ' | jq '.dest_registry.name')
        if [ "${DEST_REPOSITORY}" != "null" ]; then
          DEST_REPOSITORY=$(echo $REGISTRIES | jq " .[] | select(.name | contains($DEST_REPOSITORY))")
        fi
        # Set src_registry and dst_registry to null if unexistent.
        DATA=$(echo ${replication_rule} | jq -r '@base64d ' | jq "(if .src_registry == null then (.src_registry |= null) else (.src_registry |= $SRC_REPOSITORY) end ) | (if .dest_registry == null then (.dest_registry |= null) else (.dest_registry |= $DEST_REPOSITORY) end) | .description |= \"Automatically generated rule.\" | @base64")
        if [ -z $REPLICATION_ID ]; then
          # Replication rule does not exist, so we need to create one
          curl '{{ .Values.harbor.externalURL }}/api/v2.0/replication/policies' \
            -H 'accept: application/json' \
            -H 'content-type: application/json' \
            -H 'authorization: Basic {{ printf "admin:%s" .Values.harbor.harborAdminPassword | b64enc }}' \
            --data-raw "$(echo $DATA | jq -r '@base64d')" \
            --compressed \
            --insecure
        else
          # Update existent replication rule
          curl "{{ .Values.harbor.externalURL }}/api/v2.0/replication/policies/$REPLICATION_ID" \
            -X 'PUT' \
            -H 'accept: application/json' \
            -H 'content-type: application/json' \
            -H 'authorization: Basic {{ printf "admin:%s" .Values.harbor.harborAdminPassword | b64enc }}' \
            --data-raw "$(echo $DATA | jq -r '@base64d')" \
            --compressed \
            --insecure
        fi
      done

      echo ""
      echo "Delete removed 'Automatically generated rules'"
      for replication_rule in $(echo $REPLICATION_RULES | jq -c '.[] | select(.description == "Automatically generated rule.")  | @base64' - ); do
        REPLICATION_NAME=$(echo $replication_rule | jq -r '@base64d ' -r | jq '.name' )
        # Get registry replication rule
        REPLICATION_ID=$(echo $REPLICATION_RULES | jq ".[] | select(.name == ${REPLICATION_NAME}) | .id")
        echo -n "Validating replication rule $REPLICATION_NAME ... "
        # Validate if rule is defined in yaml file
        DELETE_AUTOMATED_RULE=true
        for replication_rule_truth in $(cat $RULES | yq eval -o=json | jq -c '.replications[] | @base64' - ); do
          REPLICATION_NAME_TRUTH=$(echo $replication_rule_truth | jq -r '@base64d' -r | jq '.name')
          if [ $REPLICATION_NAME_TRUTH == $REPLICATION_NAME ]; then
            DELETE_AUTOMATED_RULE=false
            echo "exists."
            break
          fi
        done
        # Delete non defined automated rule
        if $DELETE_AUTOMATED_RULE; then
          # Delete rule
          echo "does not exist in git source of truth. Deleting with ID $REPLICATION_ID"
          curl "{{ .Values.harbor.externalURL }}/api/v2.0/replication/policies/$REPLICATION_ID" \
            -X 'DELETE' \
            -H 'accept: application/json' \
            -H 'content-type: application/json' \
            -H 'authorization: Basic {{ printf "admin:%s" .Values.harbor.harborAdminPassword | b64enc }}'
        fi
      done

      # Recheck definitions every 10 minutes
      echo "sleeping for 10 minutes..."
      sleep 600
    done
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: apply-replication-rules
  namespace: {{ .Release.Namespace }}
  labels:
    app: registry-replication
  annotations:
    data.checksum/config: {{ .Files.Get "data/registry-replication-rules.yaml" | sha256sum }}
spec:
  selector:
    matchLabels:
      app: registry-replication
  template:
    metadata:
      labels:
        app: registry-replication
      annotations:
        data.checksum/config: {{ .Files.Get "data/registry-replication-rules.yaml" | sha256sum }}
    spec:
      containers:
      - name: apply-cve-rules
        image: registry.cern.ch/cloud/rally-magnum-containers/alpine-jq:v0.1.1
        command:
        - /bin/bash
        - "-c"
        - /tmp/script/apply-registry-replication-rules.sh
        volumeMounts:
        - name: registry-replication-rules
          mountPath: /tmp/data
        - name: registry-replication-script
          mountPath: /tmp/script
      volumes:
      - name: registry-replication-rules
        configMap:
          name: registry-replication-rules
          defaultMode: 0555
      - name: registry-replication-script
        secret:
          secretName: registry-replication-script
          defaultMode: 0555
{{- end }}
