# Registry

This repo sets the deployment and configuration for the cluster containing the
harbor helm and docker image registry

## Pre-Requisites

First get a Kubernetes cluster up and running, [check here](https://clouddocs.web.cern.ch/containers/quickstart.html).

## Deployment

!!! warning
When creating a cluster from a new template, be sure to update the
rules that syncronize the required images to BOOTSTRAP Harbor in the
registry-replication-rules.yaml file available at ./charts/kops-registry/data/registry-replication-rules.yaml

```bash
openstack coe cluster create kops-registry-xxx \
--keypair dtomasgu-key \
--cluster-template kubernetes-1.25.3-3-multi \
--master-flavor m2.medium \
--master-count 3 \
--label master_availability_zones=cern-geneva-a,cern-geneva-b,cern-geneva-c \
--flavor m2.large \
--node-count 1 \
--merge-labels \
--labels cvmfs_csi_enabled=false \
--labels eos_enabled=false \
--labels keystone_auth_enabled=false \
--labels monitoring_enabled=true \
--labels max_node_count=10 \
--labels ingress_controller=nginx \
--labels logging_producer=kubernetes \
--labels logging_include_internal=true

### Add Cluster Nodes

openstack coe nodegroup create \
  --merge-labels \
  --labels availability_zone=cern-geneva-a \
  --node-count 2 \
  --min-node 1 \
  --flavor m2.large \
  308d607d-003d-43e1-ae1d-383de2111d58 gva-a
openstack coe nodegroup create \
  --merge-labels \
  --labels availability_zone=cern-geneva-b \
  --node-count 2 \
  --min-node 1 \
  --flavor m2.large \
  308d607d-003d-43e1-ae1d-383de2111d58 gva-b
openstack coe nodegroup create \
  --merge-labels \
  --labels availability_zone=cern-geneva-c \
  --node-count 2 \
  --min-node 1 \
  --flavor m2.large \
  308d607d-003d-43e1-ae1d-383de2111d58 gva-c
```

## Create External Load Balancer
### STAGING cluster
```bash
# Create LB
openstack loadbalancer create --name kops-registry-staging --vip-network-id CERN_NETWORK
# Add dns name to LB
openstack loadbalancer set --description "registry-staging" kops-registry-staging
# Create LB listeners (http/https)
openstack loadbalancer listener create --name kops-registry-staging-https \
                                       --protocol TCP \
                                       --protocol-port 443 kops-registry-staging
openstack loadbalancer listener create --name kops-registry-staging-http \
                                       --protocol TCP \
                                       --protocol-port 80 kops-registry-staging
# Create LB Pools (http/https)
openstack loadbalancer pool create --name kops-registry-staging-https  \
                                  --lb-algorithm ROUND_ROBIN \
                                  --listener kops-registry-staging-https \
                                  --protocol PROXY
openstack loadbalancer pool create --name kops-registry-staging-http  \
                                  --lb-algorithm ROUND_ROBIN \
                                  --listener kops-registry-staging-http \
                                  --protocol PROXY
# Create LB Health Monitors (HTTP 308/HTTPS 200)
openstack loadbalancer healthmonitor create --name kops-registry-staging-https-monitor \
                                            --delay 7 \
                                            --timeout 5 \
                                            --max-retries 3 \
                                            --url-path /api/v2.0/health \
                                            --expected-codes 200 \
                                            --type HTTPS kops-registry-staging-https
openstack loadbalancer healthmonitor create --name kops-registry-staging-http-monitor \
                                            --delay 7 \
                                            --timeout 5 \
                                            --max-retries 3 \
                                            --url-path /api/v2.0/health \
                                            --expected-codes 308,200 \
                                            --type HTTP kops-registry-staging-http
# Add reverse proxy to worker nodes
kubectl get nodes | grep -e '-node-' | awk '{print $1}' | xargs -I {} kubectl label node {} role=ingress
# Add cluster nodes to the Pools
kubectl get no -owide -l role=ingress | grep -v NAME | awk {'print $1 " " $6'} | xargs -n 2 sh -c 'openstack loadbalancer member create --protocol-port 443 --address $1 --name $0 kops-registry-staging-https; openstack loadbalancer member create --protocol-port 80 --address $1 --name $0 kops-registry-staging-http'
```

### PRODUCTION cluster
```bash
# Create LB
openstack loadbalancer create --name kops-registry --vip-network-id CERN_NETWORK
# Add dns name to LB
openstack loadbalancer set --description "registry" kops-registry
# Create LB listeners (http/https)
openstack loadbalancer listener create --name kops-registry-https \
                                       --protocol TCP \
                                       --protocol-port 443 kops-registry
openstack loadbalancer listener create --name kops-registry-http \
                                       --protocol TCP \
                                       --protocol-port 80 kops-registry
# Create LB Pools (http/https)
openstack loadbalancer pool create --name kops-registry-https  \
                                  --lb-algorithm ROUND_ROBIN \
                                  --listener kops-registry-https \
                                  --protocol PROXY
openstack loadbalancer pool create --name kops-registry-http  \
                                  --lb-algorithm ROUND_ROBIN \
                                  --listener kops-registry-http \
                                  --protocol PROXY
# Create LB Health Monitors (HTTP 308/HTTPS 200)
openstack loadbalancer healthmonitor create --name kops-registry-https-monitor \
                                            --delay 7 \
                                            --timeout 5 \
                                            --max-retries 3 \
                                            --url-path /api/v2.0/health \
                                            --expected-codes 200 \
                                            --type HTTPS kops-registry-https
openstack loadbalancer healthmonitor create --name kops-registry-http-monitor \
                                            --delay 7 \
                                            --timeout 5 \
                                            --max-retries 3 \
                                            --url-path /api/v2.0/health \
                                            --expected-codes 308,200 \
                                            --type HTTP kops-registry-http
# Add reverse proxy to worker nodes
kubectl get nodes | grep -e '-node-' | awk '{print $1}' | xargs -I {} kubectl label node {} role=ingress
# Add cluster nodes to the Pools
kubectl get no -owide -l role=ingress | grep -v NAME | awk {'print $1 " " $6'} | xargs -n 2 sh -c 'openstack loadbalancer member create --protocol-port 443 --address $1 --name $0 kops-registry-https; openstack loadbalancer member create --protocol-port 80 --address $1 --name $0 kops-registry-http'
# Finally, for the cluster to be able to push images/charts to the TN registry, the cluster worker nodes need to be added to the `CLOUD INFRASTRUCTURE TN` landb-set
export OS_REGION_NAME=cern
for node in $(kubectl get no -l node-role.kubernetes.io/master!="" -o jsonpath="{.items[*].metadata.name}"); do
  openstack server set --property landb-set="CLOUD INFRASTRUCTURE TN" $node;
done
```

## Dependencies

After, install the dependencies
```bash
## WARNING!!!: This should only be installed when all networking is inplace. Otherwise we risk passing le cert request limit and be blocked.
# helm install cert-manager jetstack/cert-manager --version v1.0.1 --namespace kube-system --set installCRDs=true --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=Issuer --set ingressShim.defaultIssuerGroup=cert-manager.io

TODO: Move tls ls secrets to vault
### FINALIZING (COMMON):
The final step is dependent on the cluster you are currently installing. We need to automate the LE certificate request and renovation. For this reason, if this cluster is:
#### MAIN:
## WARNING!!!: This should only be installed when all networking is inplace. Otherwise we risk passing le cert request limit and be blocked.
# helm install cert-manager jetstack/cert-manager --version v1.7.1 --namespace kube-system --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=Issuer --set ingressShim.defaultIssuerGroup=cert-manager.io

#### SECONDARY:
First get the service account's BEARER TOKEN and CA.crt from the main cluster:
NAMESPACE=prod
SECRET_TO_SYNC=harbor-harbor-ingress
SECRET_NAME=$(kubectl -n $NAMESPACE get sa secret-sync -oyaml | yq '.secrets[0].name')
CA=$(kubectl -n $NAMESPACE get secret $SECRET_NAME -oyaml | yq '.data."ca.crt"')
TOKEN=$(kubectl -n $NAMESPACE get secret $SECRET_NAME -oyaml | yq  '.data.token')
APISERVER=https://$(kubectl -n default get endpoints kubernetes --no-headers | awk '{ print $2 }')

Now, switch to the secondary cluster and create the secret-sync job.
cat <<EOF > certificate-sync-job.yaml
apiVersion: v1
kind: Secret
metadata:
  name: secret-sync-command
  namespace: $NAMESPACE
type: Opaque
stringData:
  secret-sync.sh: |
    export NAMESPACE=$NAMESPACE
    echo $CA | base64 -d > /tmp/ca.crt
    export TOKEN=$( echo $TOKEN | base64 -d)
    curl -s $APISERVER/api/v1/namespaces/$NAMESPACE/secrets/$SECRET_TO_SYNC --header "Authorization: Bearer \$TOKEN" --cacert /tmp/ca.crt -o /tmp/secret.json
    jq 'del(.metadata.managedFields,.metadata.creationTimestamp,.metadata.resourceVersion,.metadata.uid,.metadata.selfLink,.metadata.annotations."kubectl.kubernetes.io/last-applied-configuration")' /tmp/secret.json > /tmp/tls-secret.json
    kubectl apply -f /tmp/tls-secret.json
    exit 0

---

apiVersion: batch/v1
kind: CronJob
metadata:
  name: tls-secret-sync
  namespace: $NAMESPACE
spec:
  schedule: "0 * * * *"
  failedJobsHistoryLimit: 3
  successfulJobsHistoryLimit: 1
  jobTemplate:
    spec:
      template:
        spec:
          serviceAccountName: secret-sync
          restartPolicy: OnFailure
          containers:
          - name: secret-sync
            image: registry.cern.ch/cloud/rally-magnum-containers/kubectl:v0.1.3
            command:
            - bin/ash
            - -c
            - /tmp/script/secret-sync.sh
            volumeMounts:
            - name: config-volume
              mountPath: /tmp/script
          volumes:
          - name: config-volume
            secret:
              secretName: secret-sync-command
              defaultMode: 365
EOF
kubectl apply -f certificate-sync-job.yaml
```
